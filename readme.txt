=== Plugin Name ===
Contributors: Olanipekun Olufemi
Tags: woocommerce, Paylater, Paylater , payment gateways, naira, nigeria
Requires at least: 3.0
Tested up to: 3.5.*

== Description ==

The WooCommerce Paylater Module plugin allows you to use the Paylater gateway with your WooCommerce online store. It will add the Nigerian currency and provide settings for the Paylater gateway within the WooCommerce gateway settings.

Note: Paylater is an account based payment gateway for Nigeria. To use this plugin you need to have both WooCommerce installed and activated and an account with Paylater.

== Installation ==

1. Upload the folder 'woocommerce_Paylater' to the '/wp-content/plugins/' directory

2. Activate 'WooCommerce Paylater Payment Gateway' through the 'Plugins' menu in WordPress

== Usage ==

Usage Information

1. Go to WooCommerce settings

2. Choose Checkout

3. Select Paylater 

4. Click on settings and enter your relevant Paylater account information

5. Purchase your item through the shop as normal but now with Paylater  as a payment option.