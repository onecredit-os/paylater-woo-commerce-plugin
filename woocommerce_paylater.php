<?php
/*
	Plugin Name: WooCommerce Paylater Payment Gateway
	Plugin URI: https://paylater.ng
	Description: Paylater Woocommerce Payment Gateway allows you to accept payment on your Woocommerce store via the Paylater Payment Gateway.
	Version: 1.0
	Author: Olanipekun Olufemi
	Author URI: http://dedeveloper.wordpress.com
	License:           GPL-2.0+
 	License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
*/


add_Action('plugins_loaded', 'init_wc_gateway_paylater',0);
function init_wc_gateway_paylater() {
if ( !class_exists( 'WC_Payment_Gateway' ) ) return;
	/**
 	 * Gateway class
 	 */
	class WC_Gateway_Paylater extends WC_Payment_Gateway {

		public function __construct(){
			global $woocommerce;
			$this->id		= 'paylater';
			$this->icon 		= apply_filters('woocommerce_paylater_icon', plugins_url( 'assets/images/paylater.png' , __FILE__ ));
			$this->has_fields 	= false;
			$this->liveurl 		= 'https://app.paylater.ng/gateway';
			$this->testurl 		= 'https://sandbox.paylater.ng/gateway';
			$this->method_title     = __( 'Paylater Payment Gatway', 'woocommerce' );
            $this->method_description = __( 'Paylater works by sending the user to the Paylater Payment Gateway Page to enter their payment information.', 'woocommerce' );
			

			// Load the form fields.
			$this->init_form_fields();

			// Load the settings.
			$this->init_settings();


			// Define user set variables
    		$this->title 			= $this->get_option( 'title' );
    		$this->description 		= $this->get_option( 'description' );
    		$this->testmode			= $this->get_option( 'testmode' );
    		$this->debug			= $this->get_option( 'debug' );
            $this->debug_email	    = $this->get_option('debug_email');
            $this->vendor           = $this->get_option('vendor');		
    		$this->vendorid         = $this->get_option('vendorid');
            
            // Payment listener/API hook
            //add_filter( 'init', array( $this, 'check_paylater_response' ));

			//Actions
			add_action('woocommerce_receipt_paylater', array($this, 'receipt_page'));
			
            
			if ( version_compare( WOOCOMMERCE_VERSION, '2.0', '<' ) ) {
				// Pre 2.0
				add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));
			} else {
				// 2.0
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			}
			
            $this->check_paylater_response();
            
            // Logs
			if ($this->debug=='yes') $this->log = $woocommerce->logger();
		}


        /**
         * Admin Panel Options
         **/
        public function admin_options(){
            echo '<h3>Paylater Payment Gateway</h3>';
            echo '<p>Paylater Payment Gateway allows you to accept payment through the Paylater Gateway.</p>';
            echo '<table class="form-table">';
            $this->generate_settings_html();
            echo '</table>';
        }


	       function init_form_fields() {
			$this->form_fields = array(
				'enabled' => array(
								'title' => __( 'Enable/Disable', 'woocommerce' ), 
								'type' => 'checkbox', 
								'label' => __( 'Enable Paylater', 'woocommerce' ), 
								'default' => 'yes'
							), 
				'title' => array(
								'title' => __( 'Title', 'woocommerce' ), 
								'type' => 'text', 
								'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ), 
								'default' => __( 'Paylater ', 'woocommerce' )
							),
				'description' => array(
								'title' => __( 'Description', 'woocommerce' ), 
								'type' => 'textarea', 
								'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ), 
								'default' => __("Pay via paylater;", 'woocommerce')
							),
				'vendorid' => array(
								'title' => __( 'Vendor ID', 'woocommerce' ), 
								'type' => 'text', 
								'description' => __( 'Vendor ID to you by paylater', 'woocommerce' )
							),
				'vendor' => array(
								'title' => __( 'Vendor Name', 'woocommerce' ), 
								'type' => 'text', 
								'description' => __( 'Vendor name registered with paylater', 'woocommerce' )
							),
				'testmode' => array(
								'title' => __( 'Paylater Test Mode', 'woocommerce' ), 
								'type' => 'checkbox', 
								'label' => __( 'Enable paylater Test Mode', 'woocommerce' )
							),
				'debug' => array(
								'title' => __( 'Debug', 'woocommerce' ), 
								'type' => 'checkbox', 
								'label' => __( 'Enable logging (<code>woocommerce/logs/paylater.txt</code>)', 'woocommerce' )
							),
				'debug_email' => array(
								'title' => __( 'Paylater Debug Email', 'woocommerce' ), 
								'type' => 'text', 
								'label' => __( 'Email address to send Paylater info to. Used for debugging. Blank for no email', 'woocommerce' )
							)
			);
	    
		}



		/**
		 * Get Paylater Args for passing to Paylater
		**/
		function get_paylater_args( $order ) {
			global $woocommerce;

			$order_id 		= $order->id;

			$order_total	= $order->get_total();
            $redirect_url 	= $this->get_return_url($order);//str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'WC_Gateway_Paylater', home_url( '/' ) ) );
            
            
            
            
            
            if ($this->debug=='yes') {
				$this->log->add( 'Paylater', 'Generating payment form for order #' . $order_id . '.');
			}

			// paylater Args
			$paylater_args = array(
            'vendorid' => $this->vendorid,
            'vendor' => $this->vendor,
            'email' => $order->billing_email,
            'transref' => $order_id,
            'note' => 'Purchase From '.site_url().' with Order Number '.$order_id,
            'redirecturl' => $redirect_url,
            'amount' => $order_total
            );

			$paylater_args = apply_filters( 'woocommerce_paylater_args', $paylater_args );
			return $paylater_args;
		}


	    /**
		 * Generate the Paylater Payment button link
	    **/
	    function generate_paylater_form( $order_id ) {
			global $woocommerce;

			$order = new WC_Order( $order_id );

			$paylater_adr = $this->liveurl;
			if ( $this->testmode == 'yes' ) {
				$paylater_adr = $this->testurl;
			}
            
			$paylater_args = $this->get_paylater_args( $order );

			$paylater_args_array = array();

			foreach ($paylater_args as $key => $value) {
				$paylater_args_array[] = '<input type="hidden" name="'.esc_attr( $key ).'" value="'.esc_attr( $value ).'" />';
			}

			return '<form action="'.esc_url( $paylater_adr ).'" method="post" id="paylater_payment_form" target="_top">
					' . implode('', $paylater_args_array) . '
					<input type="submit" class="button-alt" id="submit_paylater_payment_form" value="'.__('Make Payment', 'woocommerce').'" /> <a class="button cancel" href="'.esc_url( $order->get_cancel_order_url() ).'">'.__('Cancel order &amp; restore cart', 'woocommerce').'</a>
				</form>';

		}


	    /**
	     * Process the payment and return the result
	    **/
		function process_payment( $order_id ) {

			$order = new WC_Order( $order_id );
	        return array(
	        	'result' => 'success',
				'redirect'	=> $order->get_checkout_payment_url( true )
                //'redirect' => $this->get_return_url( $order )
	        );
		}


	    /**
	     * Output for the order received page.
	    **/
		function receipt_page( $order ) {
			echo '<p>'.__('Thank you for your order, please click the button below to make payment.', 'woocommerce').'</p>';
			echo $this->generate_paylater_form( $order );
		}


		/**
		 * Verify a successful Payment!
		**/
		function check_paylater_response(){
			global $woocommerce;
            if(isset($_GET['order-received'])){
                $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                $burstedurl = explode('?',$url);
                array_shift($burstedurl);
                $fixedarg = implode('&',$burstedurl);
                $fixedurl = site_url().'/?'.$fixedarg;
                wp_redirect($fixedurl);
            }else{
                $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                $burstedurl = explode('/',$url);
                if(!isset($_GET['transref']) && in_array('order-received',$burstedurl)){
                    $burstedurl2 = explode('?',$url);
                    $request = $burstedurl2[0];
                    array_shift($burstedurl2);
                    $fixedarg = implode('&',$burstedurl2);
                    $fixedurl = $request.'?'.$fixedarg;
                    wp_redirect($fixedurl);
                }
            }
            
			if(isset($_GET['transref']))
			{
			 
                $transaction_id = $_GET['transref'];
                $endpoint = "https://app.paylater.ng/gateway";
    			if ($this->testmode) {
    				$endpoint = "https://sandbox.paylater.ng/gateway";
    			}
    			
    			$endpoint .= '?format=json&transref=' . $transaction_id  . '&vendor=' . $this->vendor . '&vendorid=' . $this->vendorid;
            
				
				$json = wp_remote_get($endpoint);
				$transaction = json_decode($json['body'], true);
				$transaction_id = $transaction['transref'];
				$order_id 		= $transaction['transref'];
				$order_id 		= (int) $order_id;

		        $order 			= new WC_Order($order_id);
		        $order_total	= $order->get_total();

				$amount_paid 	= $transaction['amount'];

				if($transaction['transactioncode'] == 'P00')
				{					

					// check if the amount paid is equal to the order amount.
					if($order_total != $amount_paid)
					{
			            //after payment hook
		                do_action('paylater_after_payment', $transaction);

		                //Update the order status
						$order->update_status('on-hold', '');

						//Error Note
						$message = 'Thank you for shopping with us.<br />Your payment transaction was successful, but the amount paid is not the same as the total order amount.<br />Your order is currently on-hold.<br />Kindly contact us for more information regarding your order and payment status.';

						//Add Customer Order Note
	                    $order->add_order_note($message.'<br />Paylater Transaction ID: '.$transaction_id, 1);

	                    //Add Admin Order Note
	                    $order->add_order_note('This order is currently on hold.<br />Reason: Amount paid is less than the total order amount.<br />Amount Paid was: &#8358; '.$amount_paid.' while the total order amount is: &#8358; '.$order_total.'<br />Paylater Transaction ID: '.$transaction_id);

						// Reduce stock levels
						$order->reduce_order_stock();

						// Empty cart
						$woocommerce->cart->empty_cart();

						if ( function_exists( 'wc_add_notice' ) ) {
							wc_add_notice( $message, 'error' );

						} else { // WC < 2.1
							$woocommerce->add_error( $message );
							$woocommerce->set_messages();
						}
					}
					else
					{
		                //after payment hook
		                do_action('paylater_after_payment', $transaction);

		                if($order->status == 'processing'){
		                    $order->add_order_note('Payment Via Paylater<br />Transaction ID: '.$transaction_id);

		                    //Add customer order note
		 					$order->add_order_note('Payment Received.<br />Your order is currently being processed.<br />We will be shipping your order to you soon.<br />Paylater Transaction ID: '.$transaction_id, 1);

							// Reduce stock levels
							$order->reduce_order_stock();

							// Empty cart
							$woocommerce->cart->empty_cart();

							$message = 'Thank you for shopping with us.<br />Your transaction was successful, payment was received.<br />Your order is currently being processed.';

							if ( function_exists( 'wc_add_notice' ) ) {
								wc_add_notice( $message, 'success' );

							} else { // WC < 2.1
								$woocommerce->add_message( $message );
								$woocommerce->set_messages();
							}
		                }
		                else{
							$order->update_status('processing', 'Payment received, your order is currently being processed.');

		                    $order->add_order_note('Payment Via Paylater Payment Gateway<br />Transaction ID: '.$transaction_id);

		                    //Add customer order note
		 					$order->add_order_note('Payment Received.<br />Your order is currently being processed.<br />We will be shipping your order to you soon.<br />Paylater Transaction ID: '.$transaction_id, 1);

							// Reduce stock levels
							$order->reduce_order_stock();

							// Empty cart
							$woocommerce->cart->empty_cart();

							$message = 'Thank you for shopping with us.<br />Your transaction was successful, payment was received.<br />Your order is currently being processed.';

							if ( function_exists( 'wc_add_notice' ) ) {
								wc_add_notice( $message, 'success' );

							} else { // WC < 2.1
								$woocommerce->add_message( $message );
								$woocommerce->set_messages();
							}
		                }
	                }
				}

	            else
	            {
	            	$message = 	'Thank you for shopping with us. <br />However, the transaction wasn\'t successful, payment wasn\'t recieved. Gateway Code: '.$transaction['transactioncode'].' Gateway Status: '.$transaction['transactionstatus'];
					$transaction_id = $transaction['transaction_id'];

					//Add Customer Order Note
                    $order->add_order_note($message.'<br />Paylater Transaction ID: '.$transaction_id, 1);

                    //Add Admin Order Note
                    $order->add_order_note($message.'<br />Paylater Transaction ID: '.$transaction_id);

					if ( function_exists( 'wc_add_notice' ) )
					{
						wc_add_notice( $message, 'error' );

					}
					else // WC < 2.1
					{
						$woocommerce->add_error( $message );
						$woocommerce->set_messages();
					}
	            }
                
                if ($this->debug_email) {
            				wp_mail($this->debug_email, 'Paylater paylater Debug Feedback', print_r($_GET, true) . "\n" . $endpoint . "\n" . $json['body']);
            			}
			$redirect_url = get_permalink(woocommerce_get_page_id('myaccount'));
            wp_redirect( $redirect_url );
            exit;
            
            }

		}
	}

	/**
	 * only add the naira currency and symbol if WC versions is less than 2.1
	 */
	if ( version_compare( WOOCOMMERCE_VERSION, "2.1" ) <= 0 ) {

		/**
		* Add NGN as a currency in WC
		**/
		add_filter( 'woocommerce_currencies', 'add_my_currency' );

		if( ! function_exists( 'add_my_currency' )){
			function add_my_currency( $currencies ) {
			     $currencies['NGN'] = __( 'Naira', 'woocommerce' );
			     return $currencies;
			}
		}

		/**
		* Enable the naira currency symbol in WC
		**/
		add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

		if( ! function_exists( 'add_my_currency_symbol' ) ){
			function add_my_currency_symbol( $currency_symbol, $currency ) {
			     switch( $currency ) {
			          case 'NGN': $currency_symbol = '&#8358; '; break;
			     }
			     return $currency_symbol;
			}
		}
	}


	/**
	* Add Settings link to the plugin entry in the plugins menu for WC below 2.1
	**/
	if ( version_compare( WOOCOMMERCE_VERSION, "2.1" ) <= 0 ) {

		add_filter('plugin_action_links', 'paylater_plugin_action_links', 10, 2);

		function paylater_plugin_action_links($links, $file) {
		    static $this_plugin;

		    if (!$this_plugin) {
		        $this_plugin = plugin_basename(__FILE__);
		    }

		    if ($file == $this_plugin) {
	        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=woocommerce_settings&tab=payment_gateways&section=WC_Gateway_Paylater">Settings</a>';
		        array_unshift($links, $settings_link);
		    }
		    return $links;
		}
	}
	/**
	* Add Settings link to the plugin entry in the plugins menu for WC 2.1 and above
	**/
	else{
		add_filter('plugin_action_links', 'paylater_plugin_action_links', 10, 2);

		function paylater_plugin_action_links($links, $file) {
		    static $this_plugin;

		    if (!$this_plugin) {
		        $this_plugin = plugin_basename(__FILE__);
		    }

		    if ($file == $this_plugin) {
		        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=wc-settings&tab=checkout&section=wc_gateway_paylater">Settings</a>';
		        array_unshift($links, $settings_link);
		    }
		    return $links;
		} 
   
	}
    
    /**
 	* Add Paylater Gateway to WC
 	**/
	function woocommerce_add_paylater_gateway($methods) {
		$methods[] = 'WC_Gateway_Paylater';
		return $methods;
	}

	add_filter('woocommerce_payment_gateways', 'woocommerce_add_paylater_gateway');
}

	

